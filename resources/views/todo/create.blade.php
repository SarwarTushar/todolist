@extends('layouts.app')
@section('content')
<h1>To Do Create</h1>
<div class="card-header">
{{Form::open(['action' => 'TodoController@store', 'method' => 'POST'])}}

{{ Form::bsText('text') }}
{{ Form::bsTextArea('body') }}
{{ Form::bsText('due') }}
{{ Form::bsSubmit('submit',['class'=>'btn btn-primary']) }}

{!! Form::close() !!}
</div>
@endsection
