@extends('layouts.app')
@section('content')
<h1>To Do Edit</h1>
<div class="card-header">
{{Form::open(['action' => ['TodoController@update',$todo->id], 'method' => 'POST'])}}

{{ Form::bsText('text',$todo->text) }}
{{ Form::bsTextArea('body',$todo->body) }}
{{ Form::bsText('due',$todo->due) }}
{{ Form::hidden('_method','PUT') }}
{{ Form::bsSubmit('update',['class'=>'btn btn-primary']) }}

{!! Form::close() !!}
</div>
@endsection
