@extends('layouts.app')
@section('content')
<h1>To Do</h1>
<a class="btn btn-default" href="/">Go back</a>
    <div class="card-header">
        <h1>
            {{$todos->text}}

        </h1>
        <div class="badge bg-danger">
            {{$todos->due}}
        </div>
        <hr>
        <p>{{$todos->body}}</p>
    </div>

<br><br>
<div class="btn btn-group ">
    <div class="col-xs-6">
        <a href="/todo/{{$todos->id}}/edit" class="btn btn-secondary">Edit</a>
    </div>

    <div class="col-xs-6">
        {{Form::open(['action' => ['TodoController@destroy',$todos->id], 'method' => 'POST'])}}

        {{ Form::hidden('_method','DELETE') }}
        {{ Form::bsSubmit('Delete',['class'=>'btn btn-danger']) }}

        {!! Form::close() !!}
    </div>
</div>

@endsection
