@extends('layouts.app')
@section('content')
<h1>To Do</h1>
@if (count($todos)>0)

@foreach ($todos as $todo )
    <div class="card-header">
        <h5>
            <a style="text-decoration: none;" href="todo/{{$todo->id}}">{{$todo->text}}</a>
            <span class="badge rounded-pill bg-danger">{{$todo->due}}</span>
        </h5>
    </div>
    <hr>
@endforeach

@endif
@endsection
