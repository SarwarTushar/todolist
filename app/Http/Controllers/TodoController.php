<?php

namespace App\Http\Controllers;
use App\ToDo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todos=ToDo::orderBy('created_at','desc')->get();
        return view('todo.index')->with('todos',$todos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'text'=>'required',
            'body'=>'required'
        ]);

        $todos=new ToDo();

        $todos->text=$request->text;
        $todos->body=$request->body;
        $todos->due=$request->due;

        $todos->save();

        return redirect('/todo')->with('success','ToDo Created Successfully ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $todos=ToDo::find($id);
        return view('todo.show')->with('todos',$todos);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $todo=ToDo::find($id);
        return view('todo.edit')->with('todo',$todo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $todos=ToDo::find($id);
        $todos->text=$request->text;
        $todos->body=$request->body;
        $todos->due=$request->due;

        $todos->save();

        return redirect('/todo')->with('success','ToDo Updated Successfully ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todos=ToDo::find($id);

        $todos->delete();

        return redirect('/todo')->with('success','ToDo Deleted Successfully ');
    }
}
